<?php

use App\Models\Post;
use Illuminate\Database\Seeder;
use Carbon\Carbon;


class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Post::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Post::create([
                'title' => $faker->sentence,
                'body' => $faker->paragraph,
                'layout' => 'content_one_image', // there must be 3 types of layout : single_image, content_one_image, content_two_image
                'start_date' => Carbon::today(),
                'end_date' => $current = Carbon::now()->addDay(30)
            ]);
        }
    }
}
