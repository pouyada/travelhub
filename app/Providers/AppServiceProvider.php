<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	    Validator::extend('greater_equal_field', function($attribute, $value, $parameters, $validator) {
		    $min_field = $parameters[0];
		    $data = $validator->getData();
		    $min_value = $data[$min_field];
		    return $value >= $min_value;
	    });

	    Validator::replacer('greater_equal_field', function($message, $attribute, $rule, $parameters) {
		    return str_replace(':field', $parameters[0], $message.'. '. $attribute.' must be greater than or equal to '.$parameters[0] );
	    });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
