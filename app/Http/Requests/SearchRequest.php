<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->postValidations();
    }

    /**
     * @return array
     */
    public function postValidations(){
        return [
            //'layout'        => 'required|in:single_image,content_one_image,content_two_image',
            //'end_date'      => 'required_with:start_date|date|after:start_date'

			'region_id'         => 'bail|required',
			'hotel_ids'         => 'bail|present|array',
			'hotel_ids.*'       => 'bail|string|min:1',
			'arrival_date'      => 'bail|date|after_or_equal:today',
			'nights'            => 'bail|required|integer',
			'rooms'             => 'bail|required|array',
	        'rooms.*.adults'    => 'bail|required|integer|min:1',
	        'rooms.*.children'  => 'bail|present|array',
			'rooms.*.children.*'=> 'integer|min:1|max:17'

        ];
    }
}
