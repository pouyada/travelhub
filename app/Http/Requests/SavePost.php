<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class SavePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ($this->method() === Request::METHOD_POST) ?  $this->postValidations() :  $this->putValidations();
    }

    /**
     * @return array
     */
    public function postValidations(){
        return [
            'title'         => 'required|unique:posts|max:255',
            'body'          => 'required',
            'layout'        => 'required|in:single_image,content_one_image,content_two_image',
            'start_date'    => 'date|after_or_equal:today',
            'end_date'      => 'required_with:start_date|date|after:start_date'
        ];
    }

    /**
     * @return array
     */
    public function putValidations(){
        return [
            'title'         => 'unique:posts|max:255',
            'body'          => '',
            'layout'        => 'required|in:single_image,content_one_image,content_two_image',
            'start_date'    => 'date|after_or_equal:today',
            'end_date'      => 'required_with:start_date|date|after:start_date'
        ];
    }
}
