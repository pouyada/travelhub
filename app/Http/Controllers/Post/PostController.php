<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\SavePost;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Resources\Post as PostResource;


class PostController extends Controller
{
    /**
     * @var Post
     */
    private $Post;

    /**
     * PostController constructor.
     * @param $Post
     */
    public function __construct()
    {
        $this->Post = new Post();
    }

    /**
     * Display a listing of the resource.
     *
     * @return PostResource
     */
    public function index()
    {
        return PostResource::collection($this->Post->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  string $id
     * @return PostResource
     */
    public function show($id)
    {
        return new PostResource($this->Post->findOrFail($id));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return PostResource
     */
    public function store(SavePost $request)
    {
        $post = $this->Post->create($request->all());
        return new PostResource($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return PostResource
     */
    public function update(SavePost $request, string $id)
    {
        $post =  $this->Post->findOrFail($id);
        $post->update($request->all());

        return new PostResource($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->Post->find($id);
        !$post ?: $post->delete();

        return response()->json(null, 204);
    }


}
