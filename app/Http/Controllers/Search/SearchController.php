<?php

namespace App\Http\Controllers\Search;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;
use App\Models\Post;
use App\ThirdParty\RoomsXML\Requests\AvailabilitySearchRequest;
use App\ThirdParty\RoomsXML\Results\AvailabilitySearchResult;
use Illuminate\Http\Request;
use App\Http\Resources\SearchResource;


class SearchController extends Controller
{


    /**
     * PostController constructor.
     * @param $Post
     */
    public function __construct()
    {
        $this->Post = new Post();
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return SearchResource
     */
    public function searchAll(SearchRequest $request)
    {
        if(env('ROOMS_XML_IS_ACTIVE')){
	        $search_promise = $this->fetchFromRoomsXml($request);
	        $result = $search_promise->wait();
	        return $result ;
        }

        return [];
    }


	/**
	 * @param SearchRequest $request
	 *
	 * @return \GuzzleHttp\Promise\PromiseInterface
	 */
    private function fetchFromRoomsXml(SearchRequest $request) {

	    $availability_search = new AvailabilitySearchRequest($request);
	    $promise = $availability_search->checkAvailability();

	    $resp_promise = $promise ->then(
		    function ($response) {
			    $response = new AvailabilitySearchResult($response);
			    return $response->getArrayCopy();
		    }, function (\Exception $exception) {
		    throw new \Exception($exception->getMessage(), $exception->getCode());
	    });

	    return $resp_promise;
    }

}
