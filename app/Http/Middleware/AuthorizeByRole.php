<?php

namespace App\Http\Middleware;



use Closure;
use Illuminate\Support\Facades\Config;

class AuthorizeByRole
{
    private $user;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->user = $request->user()->toArray();

        $user_role = $this->user['role'];

        $value = Config::get('acl.resources');

        // here we should check if the user role exists in the acl array
      //  $resource = $mvcAuthEvent->getResource();

        return $next($request);
    }
}
