<?php
/**
 * Created by PhpStorm.
 * User: Pouya
 * Date: 16/04/2018
 * Time: 23:46
 */

use Spatie\ArrayToXml\ArrayToXml;


	/**
	 * @param $array
	 * @return string
	 */
	function arrayToXml(array $array, $root_element){
		$result = ArrayToXml::convert($array, $root_element);
		return $result;
	}

	/**
	 * @param string $xml_string
	 */
	function xmlToArray($xml_string) : array {
		$simpleXMLElement = simplexml_load_string($xml_string, "SimpleXMLElement", LIBXML_NOCDATA);
		if($simpleXMLElement === FALSE )
			throw new \Exception('Error while converting XML to array', 500);

		$json_string = json_encode($simpleXMLElement);
		return json_decode($json_string, TRUE);
	}