<?php

namespace App\ThirdParty\RoomsXML;


class RoomsXMLAuthority
{

    private $Org;

    private $User;

    private $Password;

    private $Language;

    private $Currency;

    private $DebugMode;

    private $TestMode;

    private $TimeOut;

    private $Version;

    /**
     * RoomsXMLAuthority constructor.
     * @param $Org
     * @param $userName
     * @param $password
     */
    public function __construct()
    {
        $this->setOrg(env('ROOMSXML_AGENCY_ID'));
        $this->setUser(env('ROOMSXML_USERNAME'));
        $this->setPassword(env('ROOMSXML_PASSWORD'));
        $this->setCurrency(env('ROOMSXML_DEFAULT_CURRENCY'));
        $this->setDebugMode('false');
        $this->setTestMode(env('ROOMSXML_TEST_MODE', true));
        $this->setLanguage(env('ROOMSXML_DEFAULT_LANG'));
        $this->setTimeOut(env('ROOMSXML_DEFAULT_TIMEOUT'));
        $this->setVersion(env('ROOMSXML_API_VERSION'));
    }


    /**
     * @return mixed
     */
    public function getOrg()
    {
        return $this->Org;
    }

    /**
     * @param mixed $Org
     */
    public function setOrg($Org)
    {
        $this->Org = $Org;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->User;
    }

    /**
     * @param mixed $User
     */
    public function setUser($User)
    {
        $this->User = $User;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->Language;
    }

    /**
     * @param mixed $Language
     */
    public function setLanguage($Language)
    {
        $this->Language = $Language;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->Currency;
    }

    /**
     * @param mixed $Currency
     */
    public function setCurrency($Currency)
    {
        $this->Currency = $Currency;
    }

    /**
     * @return mixed
     */
    public function getDebugMode()
    {
        return $this->DebugMode;
    }

    /**
     * @param mixed $DebugMode
     */
    public function setDebugMode($DebugMode)
    {
        $this->DebugMode = $DebugMode;
    }

    /**
     * @return mixed
     */
    public function getTestMode()
    {
        return $this->TestMode;
    }

    /**
     * @param mixed $TestMode
     */
    public function setTestMode($TestMode)
    {
        $this->TestMode = $TestMode;
    }

    /**
     * @return mixed
     */
    public function getTimeOut()
    {
        return $this->TimeOut;
    }

    /**
     * @param mixed $TimeOut
     */
    public function setTimeOut($TimeOut)
    {
        $this->TimeOut = $TimeOut;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->Version;
    }

    /**
     * @param mixed $Version
     */
    public function setVersion($Version)
    {
        $this->Version = $Version;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->Password;
    }

    /**
     * @param mixed $Password
     */
    public function setPassword($Password)
    {
        $this->Password = $Password;
    }

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}