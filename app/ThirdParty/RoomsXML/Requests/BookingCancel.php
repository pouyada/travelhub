<?php
/**
 * Created by PhpStorm.
 * User: pouya
 * Date: 47/4/18
 * Time: 6:45 PM
 */

namespace App\ThirdParty\RoomsXML\Requests;


use App\ThirdParty\RoomsXML\RoomsXMLRequest;

/**
 * Class BookingCreate
 *
 * @package ThirdParty\RoomsXML\Operations
 * @XmlRoot("BookingCancel")
 */
class BookingCancel extends RoomsXMLRequest
{
    /**
     * @var
     * @SerializedName("BookingId")
     * @Type(name="string")
     * @XmlElement(cdata=false)
     */
    private $BookingId;


    /**
     * @var
     * @Type(name="string")
     * @SerializedName("CommitLevel")
     * @XmlElement(cdata=false)
     */
    private $CommitLevel;

    /**
     * @param array $payLoad
     *
     * @return array
     */
    public function cancelBooking(array $payLoad): array
    {
        $this->setCommitLevel($payLoad['commitLevel']);
        $this->setBookingId($payLoad['bookingId']);
        $this->setAuthority($this->auth);
        $this->operationData = $this;

        $content = $this->sendRequest();

        return $this->getResponse($content, 'ThirdParty\RoomsXML\Results\BookingCancelResult');
    }

    /**
     * @return mixed
     */
    public function getBookingId()
    {
        return $this->BookingId;
    }

    /**
     * @param mixed $BookingId
     */
    public function setBookingId($BookingId)
    {
        $this->BookingId = $BookingId;
    }

    /**
     * @return mixed
     */
    public function getCommitLevel()
    {
        return $this->CommitLevel;
    }

    /**
     * @param mixed $CommitLevel
     */
    public function setCommitLevel($CommitLevel)
    {
        $this->CommitLevel = $CommitLevel;
    }
}
