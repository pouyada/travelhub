<?php

namespace App\ThirdParty\RoomsXML\Requests;

use App\Http\Requests\SearchRequest;
use App\ThirdParty\RoomsXML\Model\AvailabilitySearchModel;
use App\ThirdParty\RoomsXML\Model\HotelStayDetails;
use App\ThirdParty\RoomsXML\RoomsXMLRequest;
use Illuminate\Support\Collection;

/**
 * Class AvailabilitySearch
 *
 * @package App\ThirdParty\RoomsXML\Operations
 * @XmlRoot(name="AvailabilitySearch")
 */
class AvailabilitySearchRequest extends RoomsXMLRequest
{
	/**
	 * @var AvailabilitySearchModel
	 */
    private $availabilitySearchModel;

	/**
	 * @var Collection
	 */
    public $request_data;

    private $hotel_stay_details;

	/**
	 * AvailabilitySearch constructor.
	 */
	public function __construct(SearchRequest $request_data) {
		parent::__construct();
		$this->request_data = $request_data;
	}

	/**
     * @return
     * @throws \App\ThirdParty\RoomsXML\RoomsXMLException
     */
    public function checkAvailability()
    {
	    $data = $this->getAvailabilitySearchModel()->getArrayCopy();
        return $this->sendRequest('AvailabilitySearch', 'POST', $data);
    }

	/**
	 * @return AvailabilitySearchModel
	 */
    private function getAvailabilitySearchModel(){

	    if(!isset($this->availabilitySearchModel)){
		    $this->availabilitySearchModel = new AvailabilitySearchModel(
			    $this->getHotelStayDetails(),
			    $this->request_data->get( 'region_id' ),
			    $this->request_data->get( 'hotel_ids' )
		    );
	    }
	    return $this->availabilitySearchModel;
    }

	/**
	 * @return HotelStayDetails
	 */
	private function getHotelStayDetails(){

		$arrival_date   = $this->request_data->get('arrival_date');
		$nights         = $this->request_data->get('nights');
		$rooms_data     = $this->request_data->get('rooms');

		if(!isset($this->hotel_stay_details)){
			$this->hotel_stay_details = new HotelStayDetails($arrival_date, $nights, $rooms_data);
		}
		return $this->hotel_stay_details;
	}
}