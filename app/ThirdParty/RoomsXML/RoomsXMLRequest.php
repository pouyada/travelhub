<?php

namespace App\ThirdParty\RoomsXML;

use App\ThirdParty\RoomsXML\Model\HotelStayDetails;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;


/**
 * Class RoomsXMLRequest
 * @package ThirdParty\RoomsXML
 */
class RoomsXMLRequest
{
    protected $apiURL;

	/**
	 * @var array
	 */
    public $request_data;

    /**
     * @var RoomsXMLAuthority
     */
    public $authority;

    /**
     * @var HotelStayDetails
     */
    public $hotelStayDetails;

    /**
     * @var Client
     */
    public $client;

    /**
     * @param $auth_config
     */
    public function __construct()
    {
        $this->client = new Client();
    }


	/**
	 * @param $root_element
	 * @param $method
	 * @param array|null $data
	 *
	 * @return PromiseInterface
	 */
    public function sendRequest($root_element, $method, array $data = null)
    {
	    $xml_data  = arrayToXml($data, $root_element);
	    $headers = ['Content-Type' => 'text/xml; charset=UTF8'];
	    $request = new Request($method, $this->getApiUrl(), $headers, $xml_data);

	    $promise = $this->client->sendAsync($request)->then(
		    function (Response $response) {
			    $response = xmlToArray((string) $response->getBody());

			    if(isset($response['Code']) && isset($response['Description'])){
				    throw new \Exception('RoomsXML Error ' . $response['Code'] .'. '. $response['Description'], 500);
			    }
			    return $response;
		    }, function (\Exception $exception) {

		    //TODO : log the error and show a general message to user
		    throw new \Exception($exception->getMessage(), $exception->getCode());
	    });
	    return $promise;
    }

	/**
     * @return RoomsXMLAuthority
     */
    private function getAuthority()
    {
        if(!isset($this->authority)){
            $this->authority = new RoomsXMLAuthority();
        }
        return $this->authority;
    }

    private function getApiUrl(){
        if(!isset($this->apiURL)){
            $this->apiURL = env('ROOMSXML_IS_DEMO') ? env('ROOMSXML_BASE_PATH_DEMO') : env('ROOMSXML_BASE_PATH_LIVE');
        }
        return $this->apiURL;
    }

}
