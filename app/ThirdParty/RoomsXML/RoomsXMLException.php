<?php
/**
 * Created by PhpStorm.
 * User: pouya
 * Date: 3/04/18
 * Time: 10:28 AM
 */

namespace App\ThirdParty\RoomsXML;

use APP\ThirdParty\RoomsXML\RoomsXMLResponse;

/**
 * Class RoomsXMLException
 * @package APP\ThirdParty\RoomsXML
 */
class RoomsXMLException extends \Exception
{
    /**
     * RoomsXMLException constructor.
     * @param null $message
     * @param null $code
     */
    public function __construct($message = null, $code = null)
    {
        parent::__construct($message, $code);
    }
}