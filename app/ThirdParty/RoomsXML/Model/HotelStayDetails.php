<?php

namespace App\ThirdParty\RoomsXML\Model;


/**
 * Hotel stay details sent to RoomsXML on search
 * @XmlRoot(name="HotelStayDetails")
 */
class HotelStayDetails
{
    /**
     * @var string
     */
    private $ArrivalDate;

    /**
     * @var int
     */
    private $Nights;

    /**
     * @var RoomRequestModel
     */
    private $Room;

	/**
	 * HotelStayDetails constructor.
	 *
	 * @param string $arrival_date
	 * @param int $nights
	 * @param RoomRequestModel $room
	 */
	public function __construct( string $arrival_date, $nights, array $rooms_data ) {
		$this->ArrivalDate = $arrival_date;
		$this->Nights      = $nights;
		$this->setRoom($rooms_data);
	}

    /**
     * @param array $Room
     */
    public function setRoom(array $rooms_data)
    {
        $this->Room = [];
	    foreach ($rooms_data as $room_data) {
		    $room_model = new RoomRequestModel($room_data['adults'], $room_data['children']);
		    $this->Room[] = $room_model->getArrayCopy();
	    }
    }


	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}