<?php

namespace App\ThirdParty\RoomsXML\Model;

/**
 * Class HotelSearchCriteria
 * @package App\ThirdParty\RoomsXML\Model
 * @XmlRoot(name="HotelSearchCriteria")
 */
class HotelSearchCriteria
{
	CONST DETAILS_LEVEL_FULL  = 'full';
	CONST DETAILS_LEVEL_BASIC = 'basic';

    /**
     * @var string
     */
//    private $HotelName;

    /**
     * @var string
     */
//    private $HotelType;

    /**
     * @var integer
     */
//    private $MinStars;

    /**
     * @var double
     */
//    private $MinPrice;


    /**
     * @var double
     */
//    private $MaxPrice;

    /**
     * @var string
     */
    private $DetailLevel;

	/**
	 * @var
	 */
    private $AvailabilityStatus;

	/**
	 * HotelSearchCriteria constructor.
	 */
	public function __construct() {
		$this->DetailLevel        = self::DETAILS_LEVEL_BASIC;
		$this->AvailabilityStatus = 'allocation';
	}

	/**
	 * @return string
	 */
	public function getDetailLevel(): string {
		return $this->DetailLevel;
	}

	/**
	 * @param string $DetailLevel
	 */
	public function setDetailLevel( string $DetailLevel ) {
		$this->DetailLevel = $DetailLevel;
	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}