<?php
/**
 * Created by PhpStorm.
 * User: pouya
 * Date: 8/4/18
 * Time: 9:46 PM
 */

namespace App\ThirdParty\RoomsXML\Model;

/**
 * Class Result
 */
class Result
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var
     */
    private $rooms;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param mixed $rooms
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;
    }


}
