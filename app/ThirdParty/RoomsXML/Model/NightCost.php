<?php
/**
 * Created by PhpStorm.
 * User: pouya
 * Date: 2/12/16
 * Time: 8:59 PM
 */

namespace App\ThirdParty\RoomsXML\Model;


/**
 * Class NightCost
 */
class NightCost
{
    /**
     * @var
     */
    protected $night;
    /**
     * @var
     *
     */
    protected $SellingPrice;

    /**
     * @return mixed
     */
    public function getNight()
    {
        return $this->night;
    }

    /**
     * @param mixed $night
     */
    public function setNight($night)
    {
        $this->night = $night;
    }

    /**
     * @return mixed
     */
    public function getSellingPrice()
    {
        return $this->SellingPrice;
    }

    /**
     * @param mixed $SellingPrice
     */
    public function setSellingPrice($SellingPrice)
    {
        $this->SellingPrice = $SellingPrice;
    }


}