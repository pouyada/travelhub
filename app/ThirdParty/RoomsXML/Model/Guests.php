<?php

namespace App\ThirdParty\RoomsXML\Model;

class Guests
{

    /**
     * int
     */
    private $Adult;

	/**
	 * @var object
	 */
    private $Child;

	/**
	 * @return string
	 */
	public function getAdult() {
		return $this->Adult;
	}

	/**
	 * @param string $adult
	 */
	public function setAdult( $adult ) {
		$this->Adult = $adult;
	}

	/**
	 * @return object
	 */
	public function getChild(): string {
		return $this->Child;
	}

	/**
	 * @param string $child
	 */
	public function addChild( string $child ) {
		$this->Child = $child;
	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}