<?php
/**
 * Created by PhpStorm.
 * User: pouya
 * Date: 2/12/16
 * Time: 9:02 PM
 */

namespace App\ThirdParty\RoomsXML\Model;

/**
 * Class SellingPrice
 * @package App\ThirdParty\RoomsXML\Model
 * @XmlRoot("SellingPrice")
 */
class SellingPrice extends Price
{

}