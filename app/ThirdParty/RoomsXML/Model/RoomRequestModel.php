<?php

namespace App\ThirdParty\RoomsXML\Model;

/**
 * Class Room
 * @package App\ThirdParty\RoomsXML\Model
 */
class RoomRequestModel
{
    /**
     * @var array
     */
    protected $Guests;

	/**
	 * RoomRequestModel constructor.
	 *
	 * @param Guests $Guests
	 */
	public function __construct( int $adults, array $children ) {
		$this->Guests = [];
		$this->setAdults($adults);
		$this->setChilds($children);
	}


	public function setAdults(int $adults){

		for ($i = 0; $i < $adults; $i++) {
			$this->Guests['Adult'][$i] = [$i];
		}
	}

	public function setChilds(array $children)
	{
		foreach ($children as $child_age){
			$this->Guests['Child'][] = ['_attributes' => ['age' => $child_age]];
		}
	}


	public function getArrayCopy()
	{
		return get_object_vars($this);
	}



	/**
	 *  Guests
	 */
	private function getGuests2(){

		$adults = $this->request_data->get('adults');
		for ($i = 0; $i < $adults; $i++) {
			$guests->addAdult("<Adult />");
		}

		$children = $this->request_data->get('children');
		foreach ($children as $child_age) {
			$guests->addChild("<Child age=".$child_age." />");
		}

		return $guests;
	}
}