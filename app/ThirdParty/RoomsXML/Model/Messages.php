<?php
/**
 * Created by PhpStorm.
 * User: pouya
 * Date: 2/12/16
 * Time: 9:07 PM
 */

namespace App\ThirdParty\RoomsXML\Model;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\XmlList;
use JMS\Serializer\Annotation\XmlRoot;

/**
 * Class Messages
 * @package App\ThirdParty\RoomsXML\Model
 * @XmlRoot("Messages")
 */
class Messages
{

    /**
     * @var
     * @XmlList(inline=true,entry="Message")
     * @XmlElement(cdata=false)
     * @Type(name="array<App\ThirdParty\RoomsXML\Model\Message>")
     * @SerializedName("Message")
     */
    protected $message;


    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }


}