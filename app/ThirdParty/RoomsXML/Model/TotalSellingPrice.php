<?php
/**
 * Created by PhpStorm.
 * User: pouya
 * Date: 2/9/16
 * Time: 7:04 PM
 */

namespace App\ThirdParty\RoomsXML\Model;

use JMS\Serializer\Annotation\XmlRoot;

/**
 * Class TotalSellingPrice
 * @package App\ThirdParty\RoomsXML\Model
 * @XmlRoot("TotalSellingPrice")
 */
class TotalSellingPrice extends Price
{

}