<?php
/**
 * Created by PhpStorm.
 * User: pouya
 * Date: 1/30/16
 * Time: 10:02 PM
 */

namespace App\ThirdParty\RoomsXML\Model;


use JMS\Serializer\Annotation\XmlRoot;

/**
 * Class RoomType
 * @package App\ThirdParty\RoomsXML\Model
 * @XmlRoot("MealType")
 */
class MealType extends RoomData
{
}
