<?php

namespace App\ThirdParty\RoomsXML\Model;


/**
 * Class Hotel
 * @package App\ThirdParty\RoomsXML\Model
 * @XmlRoot("Hotel")
 */
class Hotel
{
    /**
     * @var
     * @XmlAttribute()
     * @SerializedName("id")
     * @Type(name="integer")
     */
    private $id;

    /**
     * @var
     * @Type(name="string")
     * @SerializedName("name")
     * @XmlAttribute()
     */
    protected $Name;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name)
    {
        $this->Name = $Name;
    }


}