<?php

namespace App\ThirdParty\RoomsXML\Model;

/**
 * Class Room
 * @package App\ThirdParty\RoomsXML\Model
 */
class Room
{
    /**
     * @var
     */
    protected $guests;

    /**
     * @var
     */
    protected $roomType;

    /**
     * @var
     */
    protected $mealType;

    /**
     * @var
     */
    protected $price;


    /**
     * @var
     */
    protected $TotalSellingPrice;


    /**
     * @var
     */
    protected $messages;

    /**
     * @var
     */
    protected $NightCost;

    /**
     * @var
     */
    protected $cancellationFees;


    /**
     * @return mixed
     */
    public function getGuests()
    {
        return $this->guests;
    }

    /**
     * @param mixed $guests
     */
    public function setGuests($guests)
    {
        $this->guests = $guests;
    }

    /**
     * @return mixed
     */
    public function getRoomType()
    {
        return $this->roomType;
    }

    /**
     * @param mixed $roomType
     */
    public function setRoomType($roomType)
    {
        $this->roomType = $roomType;
    }

    /**
     * @return mixed
     */
    public function getMealType()
    {
        return $this->mealType;
    }

    /**
     * @param mixed $mealType
     */
    public function setMealType($mealType)
    {
        $this->mealType = $mealType;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getTotalSellingPrice()
    {
        return $this->TotalSellingPrice;
    }

    /**
     * @param mixed $TotalSellingPrice
     */
    public function setTotalSellingPrice($TotalSellingPrice)
    {
        $this->TotalSellingPrice = $TotalSellingPrice;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }


    /**
     * @return mixed
     */
    public function getNightCost()
    {
        return $this->NightCost;
    }

    /**
     * @param mixed $NightCost
     */
    public function setNightCost($NightCost)
    {
        $this->NightCost = $NightCost;
    }

    /**
     * @return mixed
     */
    public function getCancellationFees()
    {
        return $this->cancellationFees;
    }

    /**
     * @param mixed $cancellationFees
     */
    public function setCancellationFees($cancellationFees)
    {
        $this->cancellationFees = $cancellationFees;
    }
}