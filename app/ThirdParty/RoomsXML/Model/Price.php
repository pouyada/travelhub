<?php
/**
 * Created by PhpStorm.
 * User: pouya
 * Date: 1/30/16
 * Time: 10:40 PM
 */

namespace App\ThirdParty\RoomsXML\Model;

/**
 * Class Price
 */
class Price
{
    /**
     * @var
     */
   private $amount;

   /**
    * @return mixed
    */
   public function getAmount()
   {
      return $this->amount;
   }

   /**
    * @param mixed $amount
    */
   public function setAmount($amount)
   {
      $this->amount = $amount;
   }



}