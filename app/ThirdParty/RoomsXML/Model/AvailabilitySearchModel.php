<?php
/**
 * Created by PhpStorm.
 * User: pouya
 * Date: 7/4/18
 * Time: 11:57 PM
 */

namespace App\ThirdParty\RoomsXML\Model;

use App\ThirdParty\RoomsXML\RoomsXMLAuthority;
use Illuminate\Support\Collection;

class AvailabilitySearchModel
{

	/**
	 * @var RoomsXMLAuthority
	 */
	private $Authority;

	/**
	 * @var int
	 */
	public $RegionId;

	/**
	 * @var string
	 */
	public $HotelId;

	/**
	 * @var array
	 */
	public $Hotels;

	/**
	 * @var HotelStayDetails
	 */
	private $HotelStayDetails;

	/**
	 * @var HotelSearchCriteria
	 */
	private $HotelSearchCriteria;

	/**
	 * @var integer
	 */
	private $MaxHotels;

	/**
	 * AvailabilityRequestModel constructor.
	 *
	 * @param HotelStayDetails $hotel_stay_details
	 * @param string $request_type
	 * @param string|null $Region_id
	 * @param string|null $hotel_id
	 * @param Collection|null $hotel_ids
	 */
	public function __construct( HotelStayDetails $hotel_stay_details,
								string $region_id = null,
								array $hotel_ids = null)
	{
		$uthority                  = new RoomsXMLAuthority();
		$this->Authority           = $uthority->getArrayCopy();
		$this->SetRequestType($region_id, $hotel_ids);
		$this->HotelStayDetails    = $hotel_stay_details->getArrayCopy();
		$hotel_search_criteria     = new HotelSearchCriteria();
		$this->HotelSearchCriteria = $hotel_search_criteria->getArrayCopy();
		$this->MaxHotels           = env('ROOMSXML_MAX_HOTELS_NUM', 60);
	}

	/**
		1- single Hotel, just HotelId
		2- single Region, just RegionId
		3- multiple Hotels, Hotels tag + RegionId
	 * @return mixed
	 */
	private function SetRequestType($region_id, $hotel_ids) {
		$request_hotel_ids = collect($hotel_ids);
		switch ($request_hotel_ids->count()){
			case 1:
				$this->setHotelId($request_hotel_ids->first());
				unset($this->RegionId);
				unset($this->Hotels);
				break;
			case 0:
				$this->setRegionId($region_id);
				unset($this->HotelId);
				unset($this->Hotels);
				break;
			default:
				$this->setHotels($hotel_ids);
				$this->setRegionId($region_id);
				unset($this->HotelId);
		}
	}

	/**
	 * @return RoomsXMLAuthority
	 */
	public function getAuthority(): RoomsXMLAuthority {
		return $this->Authority;
	}


	/**
	 * @return int
	 */
	public function getRegionId(): int {
		return $this->RegionId;
	}

	/**
	 * @param int $RegionId
	 */
	public function setRegionId( int $RegionId ) {
		$this->RegionId = $RegionId;
	}

	/**
	 * @return string
	 */
	public function getHotelId(): string {
		return $this->HotelId;
	}

	/**
	 * @param string $HotelId
	 */
	public function setHotelId( string $HotelId ) {
		$this->HotelId = $HotelId;
	}

	/**
	 * @return array
	 */
	public function getHotels(): array {
		return $this->Hotels;
	}

	/**
	 * @param array $hotels
	 */
	public function setHotels( array $hotels ) {
		$this->Hotels = [];
		foreach ($hotels as $hotel_id){
			$this->Hotels['id'][] = $hotel_id;
		}
	}

	/**
	 * @return HotelStayDetails
	 */
	public function getHotelStayDetails(): HotelStayDetails {
		return $this->HotelStayDetails;
	}

	/**
	 * @return HotelSearchCriteria
	 */
	public function getHotelSearchCriteria(): HotelSearchCriteria {
		return $this->HotelSearchCriteria;
	}

	/**
	 * @return int
	 */
	public function getMaxHotels(): int {
		return $this->MaxHotels;
	}


	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}