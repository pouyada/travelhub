<?php

namespace App\ThirdParty\RoomsXML\Model;


class RoomData
{

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $text;

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

}