<?php
/**
 * Created by PhpStorm.
 * User: pouya
 * Date: 2/9/16
 * Time: 5:39 PM
 */

namespace App\ThirdParty\RoomsXML\Results;


/**
 * Class BookingCreateResult
 *
 * @package App\ThirdParty\RoomsXML\Results
 * @XmlRoot("BookingCreateResult")
 */
class BookingCreateResult
{
    /**
     * @var
     * @SerializedName("CommitLevel")
     * @Type(name="string")
     */
    private $CommitLevel;

    /**
     * @var
     * @SerializedName("Booking")
     * @Type(name="App\ThirdParty\RoomsXML\Model\Booking")
     */
    private $Booking;

    /**
     * @return mixed
     */
    public function getCommitLevel()
    {
        return $this->CommitLevel;
    }

    /**
     * @param mixed $CommitLevel
     */
    public function setCommitLevel($CommitLevel)
    {
        $this->CommitLevel = $CommitLevel;
    }

    /**
     * @return mixed
     */
    public function getBooking()
    {
        return $this->Booking;
    }

    /**
     * @param mixed $Booking
     */
    public function setBooking($Booking)
    {
        $this->Booking = $Booking;
    }
}