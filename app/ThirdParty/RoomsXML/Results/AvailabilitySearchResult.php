<?php

namespace App\ThirdParty\RoomsXML\Results;

use Illuminate\Support\Facades\Storage;
/**
 * Class AvailabilitySearchResult
 */
class AvailabilitySearchResult
{

    protected $currency;
    protected $available_hotels;

	/**
	 * AvailabilitySearchResult constructor.
	 *
	 * @param $hotel_availability
	 * @param $currency
	 * @param $warning
	 * @param $test_mode
	 */
	public function __construct( array $search_result ) {

		$this->currency           = env('ROOMSXML_DEFAULT_CURRENCY');
		$this->available_hotels   = [];

		if(isset($search_result['HotelAvailability'])) {
			foreach ( $search_result['HotelAvailability'] as $hotel ) {
				$availableHotel           = new AvailableHotel( $hotel['Hotel'], $hotel['Result'] );
				$this->available_hotels[] = $availableHotel->getArrayCopy();
			}
		}
	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}


class AvailableHotel {

	private $provider;
	private $hotel;
	private $results;

	/**
	 * AvailableHotel constructor.
	 *
	 * @param $hotel_info
	 * @param $result
	 */
	public function __construct( $hotel_info, $results ) {
		$this->provider           = 'RoomsXML';
		$this->setHotel( $hotel_info );
		$this->setResults( $results );
	}

	/**
	 * @param mixed $hotel
	 */
	public function setHotel( $hotel_info ) {
		$this->hotel['Id'] = $hotel_info['@attributes']['id'];
		$this->hotel['Name'] = $hotel_info['@attributes']['name'];

		$offline_file_path = rtrim( env( 'ROOMSXML_OFFLINE_DATA_PATH' ), '/' ) . '/' . $hotel_info['@attributes']['id'] . '.xml';

		if(Storage::exists($offline_file_path)) {
			$offline_hotel_data = Storage::get($offline_file_path);
			$offline_hotel_data = xmlToArray($offline_hotel_data);

			//TODO map offline data to a class
			$this->hotel = $offline_hotel_data;
		}
	}

	/**
	 * @param mixed $results
	 */
	public function setResults( $results ) {

		if (isset($results["@attributes"]) && isset($results["@attributes"]['id'])){
			$this->results[0]['id'] = $results["@attributes"]['id'];
			$this->addRoomsToResult( $results, 0 );
		}
		elseif (is_array($results)) {
			$results_index = 0;
			foreach ( $results as $result ) {
				$this->results[$results_index]['id'] = $result['@attributes']['id'];
				$this->addRoomsToResult( $result, $results_index );
				$results_index ++;
			}
		}
	}

	/**
	 * @param $result
	 * @param $results_index
	 */
	private function addRoomsToResult( $result, $results_index ) {

		if(count($result['Room']) == 3 && isset($result['Room']['RoomType'])){
			$this->results[ $results_index ]['rooms'][0]['room_type'] = $result['Room']['RoomType']['@attributes'];
			$this->results[ $results_index ]['rooms'][0]['meal_type'] = $result['Room']['MealType']['@attributes'];
			$this->results[ $results_index ]['rooms'][0]['price']     = $result['Room']['Price']['@attributes'];
		}
		else{
			$room_index = 0;
			foreach (  $result['Room'] as $room ) {
				$this->results[ $results_index ]['rooms'][$room_index]['room_type'] = $room['RoomType']['@attributes'];
				$this->results[ $results_index ]['rooms'][$room_index]['meal_type'] = $room['MealType']['@attributes'];
				$this->results[ $results_index ]['rooms'][$room_index]['price']     = $room['Price']['@attributes'];
				$room_index ++;
			}
		}
	}

	/**
	 * @return array
	 */
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}

}