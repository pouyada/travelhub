<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// issue token/refresh token
Route::post('/oauth/token', [
    'uses' => '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken',
    'middleware' => 'throttle'
]);

// destroy token
Route::delete('/oauth/token/{token_id}', [
    'uses' => '\Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@destroy'
])->middleware('auth:api');



Route::group(['namespace' => 'Post', 'middleware' => ['auth:api','authorizeByRole']], function ($group) {

    $group->get('/posts', 'PostController@index');

    $group->get('/posts/{id}', 'PostController@show');

    $group->post('/posts', 'PostController@store');

    $group->put('/posts/{id}', 'PostController@update');

    $group->delete('/posts/{id}', 'PostController@destroy');

});

Route::group(['namespace' => 'Search', 'middleware' => ['auth:api']], function ($group) {
    $group->post('/search', 'SearchController@searchAll');
});



// all invalid routes will return an error message
$router->get('{path?}', function($path)
{
    return response()->json(
        ["error" => "Route mismatch for: /$path"],404);
})->where('path', '.*');

