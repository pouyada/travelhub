<?php
$noRightsRule = array(

);

$readOnlyRules = array(
    'GET' => true
);

$readWriteRules = array(
    'GET' => true,
    'POST' => true,
    'PUT' => true,
    'DELETE' => true,
    'PATCH' => true
);

return [

    'resources' => [
        'Machines\V1\Rest\Posts\Controller::entity' => [
            'privileges_by_role' => [
                'Customer' => $readOnlyRules,
                'Administrator' => $readWriteRules,
                'Sales' => $noRightsRule
            ]
        ],
        'Machines\V1\Rest\Posts\Controller::collection' => [
            'privileges_by_role' => [
                'Customer' => $readOnlyRules,
                'Administrator' => $readWriteRules,
                'Sales' => $noRightsRule
            ]
        ],
        'Machines\V1\Rest\Search\Controller::collection' => [
            'privileges_by_role' => [
                'Customer' => $readOnlyRules,
                'Administrator' => $readWriteRules,
                'Sales' => $noRightsRule
            ]
        ],
    ]

];
